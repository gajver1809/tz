<?php

namespace tz\classes;

/**
 * @property Seller $Seller
 * @property int $type
 */

class Client extends Person
{

	public readonly int $mobile;
	public readonly string $email;

	public function __construct(int $id){
		parent::__construct($id);
		$this->name = 'Client';
		$this->type = 4;
		$this->mobile = $this->preparePhone();
		$this->email = $this->prepareEmail();
	}

	/**
	 * @param string $name
	 *
	 * @return Seller|void
	 * @throws exceptions\PersonException
	 */
	public function __get(string $name )
	{
		if ($name === 'Seller') {
			return Seller::getById( $this->id );
		}

		if( $name == 'type' ){
			return $this->$name;
		}
	}

	public function preparePhone() : int
	{
		return 89999999999;
	}

	public function prepareEmail() : string
	{
		return 'some@email.com';
	}
}