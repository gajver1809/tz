<?php

namespace tz\classes;

use tz\classes\enums\MessageTypes;
use tz\classes\enums\NotificationEvents;
use tz\classes\interfaces\INotificationTemplate;

class ClientNotifyByEmail extends NotifyByEmail
{
	public function send(INotificationTemplate $obj): bool
	{
		$this->data = $obj->getData();

		if( !empty( $this->from ) && !empty( $this->to ) ){
			foreach( $this->to as $email ){
				return MessagesClient::sendMessage([
					MessageTypes::Email->name => [ // MessageTypes::EMAIL
						'emailFrom' => $this->from,
						'emailTo'   => $email,
						'subject'   => __('complaintClientEmailSubject', $obj->reseller->id,  $this->data ),
						'message'   => __('complaintClientEmailBody', $obj->reseller->id,  $this->data ),
					],
				],
					$obj->reseller->id,
					NotificationEvents::CHANGE_RETURN_STATUS,
					$this->data['CLIENT_ID'],
					$obj->getStatuses()['TO']
				);
			}
		}
		return false;
	}
}