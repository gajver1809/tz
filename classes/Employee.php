<?php

namespace tz\classes;

class Employee extends Contractor
{
	public function __construct(int $id){
		parent::__construct($id);
		$this->name = 'Employee';
		$this->type = 2;
	}
}