<?php

namespace tz\classes;

use tz\classes\enums\MessageTypes;
use tz\classes\enums\NotificationEvents;
use tz\classes\interfaces\INotificationTemplate;

class EmployersNotifyByEmail extends NotifyByEmail
{

	public function send(INotificationTemplate $obj): bool
	{
		$this->data = $obj->getData();

		if( !empty( $this->from ) && !empty( $this->to ) ){
			foreach( $this->to as $email ){
				return MessagesClient::sendMessage([
					MessageTypes::Email->name => [ // MessageTypes::EMAIL
						'emailFrom' => $this->from,
						'emailTo'   => $email,
						'subject'   => __('complaintEmployeeEmailSubject', $obj->reseller->id,  $this->data ),
						'message'   => __('complaintEmployeeEmailBody', $obj->reseller->id,  $this->data ),
					],
				],
					$obj->reseller->id,
					NotificationEvents::CHANGE_RETURN_STATUS
				);

			}
		}
		return false;
	}
}