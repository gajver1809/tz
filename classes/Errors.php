<?php

namespace tz\classes;

class Errors
{
	private static array $errors = [];

	/**
	 * @return bool
	 */
	public static function hasErrors() : bool
	{
		return count( self::$errors ) > 0;
	}

	/**
	 * @param string $key
	 *
	 * @return bool
	 */
	public static function hasError( string $key ) : bool
	{
		return isset( self::$errors[$key] );
	}

	/**
	 * @param        $key
	 * @param string $message
	 *
	 * @return void
	 */
	public static function addError( $key, string $message = '') : void
	{
		self::$errors[$key] = $message;
	}

	/**
	 * @return array
	 */
	public static function getErrors() : array
	{
		return self::$errors;
	}

	public static function toJson()
	{
		return json_encode( self::$errors );
	}

	public static function Reset() : void
	{
		self::$errors = [];
	}
}