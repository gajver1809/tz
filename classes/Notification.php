<?php

namespace tz\classes;


use tz\classes\interfaces\INotificationTemplate;
use tz\classes\interfaces\INotificator;

class Notification
{

	public function __construct(
		private readonly INotificationTemplate $data,
		private INotificator $sender
	){}

	/**
	 * @param INotificator $sender
	 *
	 * @return void
	 */
	public function changeSender( INotificator $sender ):void
	{
		$this->sender = $sender;
	}

	/**
	 * @return bool
	 */
	public function send() : bool
	{
		return $this->sender->send( $this->data );
	}
}