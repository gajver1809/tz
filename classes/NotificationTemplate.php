<?php

namespace tz\classes;

use tz\classes\enums\NotificationTypes;
use tz\classes\enums\Status;
use tz\classes\exceptions\NotificationException;
use tz\classes\exceptions\PersonException;
use tz\classes\interfaces\INotificationTemplate;

class NotificationTemplate implements INotificationTemplate
{
	public readonly int $notify_type;
	public readonly Seller $reseller;
	public readonly Client $client;
	private readonly array $data;
	private array $template_data = [];

	/**
	 * @param Seller $reseller
	 * @param Client $client
	 * @param array  $data
	 *
	 * @throws NotificationException
	 * @throws PersonException
	 */
	public function __construct( Seller $reseller, Client $client, array $data )
	{
		$this->notify_type = $data['notificationType'];
		$this->reseller = $reseller;
		$this->client = $client;
		$this->data = $data;
		$this->template_data = $this->makeTemplate();
	}

	/**
	 * @return array
	 */
	public function getData() : array
	{
		return $this->template_data;
	}

	/**
	 * @return bool
	 */
	public function typeChanged(): bool
	{
		return $this->notify_type == NotificationTypes::CHANGE->value && !empty($this->data['differences']['from'] ) && !empty($this->data['differences']['to']);
	}


	/**
	 * @return array
	 */
	public function getStatuses() : array
	{
		return $this->template_data['DIFFERENCES']['Status'];
	}

	/**
	 * @return array
	 * @throws NotificationException
	 * @throws PersonException
	 */
	private function makeTemplate() : array
	{
		return [
			'COMPLAINT_ID'       => $this->data['complaintId'],
			'COMPLAINT_NUMBER'   => $this->data['complaintNumber'],
			'CREATOR_ID'         => $this->data['creatorId'],
			'CREATOR_NAME'       => Employee::getById( $this->data['creatorId'] )->getFullName(),
			'EXPERT_ID'          => $this->data['expertId'],
			'EXPERT_NAME'        => Employee::getById( $this->data['expertId'] )->getFullName(),
			'CLIENT_ID'          => $this->data['clientId'],
			'CLIENT_NAME'        => $this->client->getFullName(),
			'CONSUMPTION_ID'     => $this->data['consumptionId'],
			'CONSUMPTION_NUMBER' => $this->data['consumptionNumber'],
			'AGREEMENT_NUMBER'   => $this->data['agreementNumber'],
			'DATE'               => $this->data['date'],
			'DIFFERENCES'        => $this->makeDifferences()
		];
	}

	/**
	 * @return array
	 * @throws NotificationException
	 */
	private function makeDifferences() : array
	{
		if ( $this->notify_type == NotificationTypes::NEW->value ) {
			return __('NewPositionAdded', $this->reseller->id );
		}
		elseif ( $this->typeChanged() ) {
			return __(
				'PositionStatusHasChanged',
				$this->reseller->id,
				[
					'FROM' => Status::getByID($this->data['differences']['from']),
					'TO'   => Status::getByID($this->data['differences']['to']),
				]
			);
		}
		else throw new NotificationException("The differences request field should not be empty if this is a repeat alert!");
	}

}