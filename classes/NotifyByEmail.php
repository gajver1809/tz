<?php

namespace tz\classes;

use tz\classes\interfaces\INotificator;
use tz\classes\interfaces\INotificationTemplate;

abstract class NotifyByEmail implements INotificator
{

	public function __construct(
		protected readonly string $from = '',
		protected readonly array $to = [],
		protected array $data = []
	){}

	/**
	 * @param INotificationTemplate $obj
	 *
	 * @return bool
	 */
	public abstract function send( INotificationTemplate $obj ): bool;
	
}