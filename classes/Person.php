<?php

namespace tz\classes;

use tz\classes\exceptions\PersonException;

class Person
{

	protected int $id;
	protected string $name;
	protected int $type;

	public function __construct( int $id )
	{
		$this->id = $id;
	}

	/**
	 * @param int $id
	 *
	 * @return static
	 * @throws PersonException
	 */
	public static function getById(int $id): static
	{
		$inst = new static($id);
		if( $inst->id != $id ){
			throw new PersonException("It is impossible to get a user with this ID = ".$id);
		}
		return $inst; // fakes the getById method
	}

	/**
	 * @return string|false
	 * @throws PersonException
	 */
	public function getFullName(): string|false
	{
		if( !empty( $this->name ) && !empty( $this->id ) ){
			return trim($this->name) . ' ' . $this->id;
		}
		else throw new PersonException("Name is empty!");
	}
}