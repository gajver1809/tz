<?php

namespace tz\classes;

abstract class ReferencesOperation
{
	abstract public function doOperation(): array;
}