<?php

namespace tz\classes;

use tz\classes\interfaces\IRequest;

class ReferencesOperationRequest implements IRequest
{
	public function getFieldByName( string $key ) : array
	{
		return !empty( $_REQUEST[$key] ) && is_array( $_REQUEST[$key] ) ? $_REQUEST[$key] : [];
	}
}