<?php

namespace tz\classes;

use tz\classes\interfaces\IValidator;

class ReferencesOperationValidator implements IValidator
{

	private array $validated = [];
	private array $fields = [];

	/**
	 * @param array $data
	 *
	 * @return void
	 */
	public function handle( array &$data = [] ) : void
	{
		if( empty( $data ) ) {
			Errors::addError('data', 'Empty data!');
			return;
		}

		if( empty( $this->fields ) ){
			Errors::addError('require_fields', 'There is not a single field for validation!');
			return;
		}

		foreach( $this->fields as $key => $callback ){

			if( !isset( $data[$key] ) ){
				Errors::addError('Field ' . $key . ' is missing in request!');
				continue;
			}

			if( !is_callable( $callback, true ) ){
				Errors::addError( $key, 'Function is not correct!' );
				continue;
			}

			$value = call_user_func( $callback, $data[$key] );

			if( $value ){
				$this->validated[$key] = $value;
			}
			else {
				Errors::addError( $key, 'The field has not been validated!' );
			}
		}
	}

	/**
	 * @param string       $key
	 * @param string|array $callback
	 *
	 * @return IValidator
	 */
	public function addField( string $key, string|array $callback ) : IValidator
	{
		$this->fields[$key] = $callback;
		return $this;
	}

	/**
	 * @param string $key
	 *
	 * @return IValidator
	 */
	public function removeField(string $key ) : IValidator
	{
		if( !isset( $this->fields[$key] )){
			unset( $this->fields[$key] );
		}
		return $this;
	}

	/**
	 * @return array
	 */
	public function getValidated() : array
	{
		return $this->validated;
	}

	/**
	 * @param mixed $value
	 *
	 * @return false|int
	 */
	public function prepareInt( mixed $value ): false|int
	{
		if( !empty( $value ) ){
			$value = (int)$value;
			if( $value > 0 ) return $value;
		}
		return false;
	}

	/**
	 * @param mixed $value
	 *
	 * @return false|string
	 */
	public function prepareString( mixed $value ) : false|string
	{
		if( !empty( $value ) && is_string( $value ) ){
			return htmlentities( trim($value) );
		}
		return false;
	}

}