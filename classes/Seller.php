<?php

namespace tz\classes;

/**
 * @property int $id
 */
class Seller extends Contractor
{
	public function __construct(int $id){
		parent::__construct($id);
		$this->name = 'Seller';
		$this->type = 1;
	}

	/**
	 * @param string $name
	 *
	 * @return string|void
	 */
	public function __get(string $name)
	{
		if( $name == 'id' ){
			return $this->$name;
		}
	}
}