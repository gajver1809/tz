<?php

namespace tz\classes;

use tz\classes\enums\ContractorTypes;
use tz\classes\exceptions\NotificationException;
use tz\classes\exceptions\PersonException;
use tz\classes\exceptions\ValidatorException;
use tz\classes\interfaces\IRequest;
use tz\classes\interfaces\IValidator;

class TsReturnOperation
{

	private array $result = [
		'notificationEmployeeByEmail' => false,
		'notificationClientByEmail'   => false,
		'notificationClientBySms'     => [
			'isSent'  => false,
			'message' => '',
		],
	];

	public function __construct(
		private readonly IRequest $request,
		private readonly IValidator $validation
	)
	{

	}

	public function doOperation() : array
	{

		$data = $this->request->getFieldByName('data');

		try {
			$this->validation->addField('resellerId',[ $this->validation::class , 'prepareInt'])
			                 ->addField('notificationType', [ $this->validation::class , 'prepareInt' ])
			                 ->addField('complaintId', [ $this->validation::class , 'prepareInt' ])
			                 ->addField('complaintNumber', [ $this->validation::class , 'prepareString' ])
			                 ->addField('creatorId',[ $this->validation::class , 'prepareInt' ])
			                 ->addField('expertId',[ $this->validation::class , 'prepareInt' ])
			                 ->addField('clientId',[ $this->validation::class , 'prepareInt' ])
			                 ->addField('consumptionId',[ $this->validation::class , 'prepareInt' ])
			                 ->addField('consumptionNumber',[ $this->validation::class , 'prepareString' ])
			                 ->addField('agreementNumber',[ $this->validation::class , 'prepareString' ])
			                 ->addField('date', [ $this->validation::class , 'prepareString' ])
			                 ->Handle( $data );


			if( Errors::hasError('resellerId') ){
				$result['notificationClientBySms']['message'] = 'Empty resellerId';
				return $result;
			}

			if( Errors::hasErrors() ) {
				throw new ValidatorException('Validation errors: '.Errors::toJson(), 404);
			}

			$correct_data = $this->validation->getValidated();
			if( isset( $data['differences'] ) ) $correct_data['differences'] = $data['differences'];

			$reseller = Seller::getById( $correct_data['resellerId'] );
			$client = Client::getById( $correct_data['clientId'] );

			if ( ContractorTypes::check( $client->type ) || $client->Seller->id !== $reseller->id ) {
				throw new PersonException('Client not found!', 404);
			}

			$this->sending( $reseller, $client, $correct_data );

		}
		catch( PersonException | ValidatorException | NotificationException $e ){
			return ['error' => $e->getMessage()];
		}


		return $this->result;
	}

	/**
	 * @param Seller $reseller
	 * @param Client $client
	 * @param array  $correct_data
	 *
	 * @return void
	 * @throws NotificationException
	 * @throws PersonException
	 */
	private function sending( Seller $reseller, Client $client, array $correct_data ) : void
	{
		$from = getResellerEmailFrom();
		$to = getEmailsByPermit($reseller->id, 'tsGoodsReturn');

		$notify_temp = new NotificationTemplate( $reseller, $client, $correct_data );
		$employers = new Notification( $notify_temp,  new EmployersNotifyByEmail( $from, $to ) );
		$client = new Notification( $notify_temp, new ClientNotifyByEmail( $from, [$client->email] ) );

		if( $employers->send() ){
			$this->result['notificationEmployeeByEmail'] = true;
		}

		if( $notify_temp->typeChanged() ){

			//BY EMAIL
			if( $client->send() ){
				$this->result['notificationClientByEmail'] = true;
			}

			//BY SMS
			$client->changeSender( new NotifyBySMS() );

			if ( $client->send()) {
				$this->result['notificationClientBySms']['isSent'] = true;
			}
			else {
				$this->result['notificationClientBySms']['message'] = 'Some happened!';
			}

		}

	}

}