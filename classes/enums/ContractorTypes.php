<?php

namespace tz\classes\enums;

enum ContractorTypes : int
{
	case Contractor = 0;
	case Seller = 1;
	case Employee = 2;

	public static function check( int $type ): bool
	{
		foreach( ContractorTypes::cases() as $contractor_type ){
			if( $contractor_type == $type ) return true;
		}
		return false;
	}
}