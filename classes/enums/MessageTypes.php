<?php

namespace tz\classes\enums;

enum MessageTypes : int
{
	case Email = 0;
	case SMS = 1;
}