<?php

namespace tz\classes\enums;

enum NotificationTypes : int
{
	case NEW = 1;
	case CHANGE = 2;
}
