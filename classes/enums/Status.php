<?php

namespace tz\classes\enums;

enum Status : int
{
	case Pending = 1;
	case Rejected = 2;
	case Completed = 3;

	public static function getByID( int $id ) : string
	{
		foreach (Status::cases() as $case) {
			if( $case->value == $id ){
				return $case->name;
			}
		}
		return false;
	}
}