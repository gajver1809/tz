<?php

namespace tz\classes\interfaces;

interface INotificationTemplate
{
	public function typeChanged();
	public function getStatuses();
	public function getData();
}