<?php

namespace tz\classes\interfaces;

interface INotificator
{
	public function send( INotificationTemplate $obj ) : bool;
}