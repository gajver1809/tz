<?php

namespace tz\classes\interfaces;

interface IRequest
{
	public function getFieldByName( string $key );
}