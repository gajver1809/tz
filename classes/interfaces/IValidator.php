<?php

namespace tz\classes\interfaces;

interface IValidator
{
	public function handle( array &$data = [] );
	public function addField( string $key, string|array $callback ) : IValidator;
	public function removeField( string $key ): IValidator;
	public function prepareInt( mixed $value ) : int|false;
	public function prepareString( mixed $value ): string|false;
	public function getValidated();
}