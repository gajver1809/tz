<?php
include "vendor/autoload.php";

use tz\classes\ReferencesOperationRequest;
use tz\classes\ReferencesOperationValidator;
use tz\classes\TsReturnOperation;

$validator         = new ReferencesOperationValidator();
$request           = new ReferencesOperationRequest();
$TsReturnOperation = new TsReturnOperation( $request, $validator );

$result = $TsReturnOperation->doOperation();
print_r($result);

function __( string $str, int $id, array $value = []) : array
{
	return [
		'Message' => $str,
		'Reseller' => $id,
		'Status' => $value
	];
}

function getResellerEmailFrom() : string
{
	return 'contractor@example.com';
}

function getEmailsByPermit($resellerId, $event) : array
{
	// fakes the method
	return ['someemeil@example.com', 'someemeil2@example.com'];
}
